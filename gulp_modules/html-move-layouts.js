/* ==== IMPORT PARAMS ==== */

'use strict';

/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
inDev = 'development',
inPub = 'public';
/* ==== ----- ==== */


/* ==== Replace URL or Links ==== */
const __cfg = {
 include: {
		prefix: '<!-- @@',
		suffix: '-->',
		basepath: '@file'
 }
};
/* ==== ----- ==== */


module.exports = ( nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig ) =>
	() => combiner(
	
			src(`${inDev}/*.html`),
			_run.include(__cfg.include),
			dest(inPub)
			
			).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));

  